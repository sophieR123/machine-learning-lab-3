import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;

import java.io.FileReader;
import java.util.Random;

public class WekaTools {

    public double accuracy(Classifier c, Instances test) throws Exception {
        double correct = 0;

        for (Instance instance : test){
            double answer = instance.value(instance.numAttributes()-1);
            if (c.classifyInstance(instance) == answer){
                correct++;
            }
        }
        return (correct/test.numInstances())*100;
    }

    public Instances loadClassificationData(String fullPath){
        Instances instances = null;

        try{
            FileReader reader = new FileReader(fullPath);
            instances = new Instances(reader);
        }catch(Exception e){
            System.out.println("Exception caught: "+e);
        }

        instances.setClassIndex(instances.numAttributes()-1);
        instances.numInstances();
        return instances;
    }

    public Instances[] splitData(Instances all, double proportion){
        Random rand = new Random();
        all.randomize(rand);
        Instances[] split = new Instances[2];
        //Copy all data to split[0]
        split[0]=new Instances(all);
        //Copy header data but no instances to split[1]
        split[1]=new Instances(all,0);

        for (int i = 0; i < (int) (proportion*all.numInstances()); i++){
            split[1].add(split[0].get(i));
            split[0].delete(i);
        }

        return split;
    }

    public double[] classDistribution(Instances data){
        double[] distribution = new double[data.attribute(data.numAttributes()-1).numValues()];

        for (Instance instance: data){
            distribution[(int) instance.value(instance.numAttributes()-1)]++;
        }
        return distribution;
    }

    public int[][] confusionMatrix(int[] predicted, int[] actual){
        int[][] confusionMatrix = new int[2][2];
        for (int i =0; i < predicted.length; i++) {
            confusionMatrix[actual[i]][predicted[i]]++;
        }
        return confusionMatrix;
    }

    public int[] classifyInstances(Classifier c, Instances test) throws Exception {
        int[] classifications = new int[test.numInstances()];
        for(int i = 0; i < test.numInstances(); i++) {
            classifications[i] = (int) c.classifyInstance(test.instance(i));
        }
        return classifications;
    }

    public int[] getClassValues(Instances data){
        int[] classValues = new int[data.numInstances()];
        for(int i = 0; i < data.numInstances(); i++) {
            classValues[i] = (int) data.get(i).classValue();
        }
        return classValues;
    }

}
