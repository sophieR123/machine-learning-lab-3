import weka.core.Instance;
import weka.core.Instances;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws Exception {
        WekaTools wekaTools = new WekaTools();

        lab3Question1(wekaTools);
        lab3Question3(wekaTools);
        lab3Question5a(wekaTools);
        lab3Question5b(wekaTools);
    }

    public static void lab3Question1(WekaTools wekaTools) throws Exception {
        System.out.println("Question 1");
        //Question 1 a

        int[] actual={0,0,1,1,1,0,0,1,1,1};
        int[] predicted={0,1,1,1,1,1,1,1,1,1};
        int[][] confusionMatrix = wekaTools.confusionMatrix(predicted, actual);

        System.out.println("Confusion Matrix");
        System.out.println("  0 1");
        System.out.println("0 " + confusionMatrix[0][0] + " " + confusionMatrix[1][0]);
        System.out.println("1 " + confusionMatrix[0][1] + " " + confusionMatrix[1][1]);
        System.out.println("Confusion Matrix: " + Arrays.deepToString(confusionMatrix));

        // Question 1 b & c
        Instances football = wekaTools.loadClassificationData("FootballPlayers.arff");
        OneNN oneNN = new OneNN();
        Instances[] splitData = wekaTools.splitData(football, 0.3);
        Instances footballTrain = splitData[0];
        Instances footballTest = splitData[1];

        oneNN.buildClassifier(footballTrain);
        int[] predictedFootball = wekaTools.classifyInstances(oneNN, footballTest);
        int[] actualFootball = wekaTools.getClassValues(footballTest);

        System.out.println("Classify instances: " + Arrays.toString(predictedFootball));
        System.out.println("Class Values: " + Arrays.toString(actualFootball));

        // Question 1 d
        int[][] footballCMatrix = wekaTools.confusionMatrix(predictedFootball, actualFootball);
        System.out.println("Football Confusion Matrix");
        System.out.println("  0 1");
        System.out.println("0 " + footballCMatrix[0][0] + " " + footballCMatrix[1][0]);
        System.out.println("1 " + footballCMatrix[0][1] + " " + footballCMatrix[1][1]);
        System.out.println("Football Confusion Matrix: " + Arrays.deepToString(footballCMatrix));


    }

    public static void lab3Question3(WekaTools wekaTools) throws Exception {
        System.out.println("Question 3");
        //Question 3 a
        weka.classifiers.rules.ZeroR zeroR = new weka.classifiers.rules.ZeroR();

        Instances football = wekaTools.loadClassificationData("FootballPlayers.arff");
        Instances[] splitData = wekaTools.splitData(football, 0.4);
        Instances footballTrain = splitData[0];
        Instances footballTest = splitData[1];

        zeroR.buildClassifier(footballTrain);
        int[] predictedZero = wekaTools.classifyInstances(zeroR, footballTest);
        int[] actualZero = wekaTools.getClassValues(footballTest);

        int[][] zeroRConfusion = wekaTools.confusionMatrix(predictedZero, actualZero);
        System.out.println("Zero Confusion Matrix");
        System.out.println("  0 1");
        System.out.println("0 " + zeroRConfusion[0][0] + " " + zeroRConfusion[1][0]);
        System.out.println("1 " + zeroRConfusion[0][1] + " " + zeroRConfusion[1][1]);
        System.out.println("Zero Confusion Matrix: " + Arrays.deepToString(zeroRConfusion));


    }

    public static void lab3Question5a(WekaTools wekaTools) throws Exception {
        System.out.println("Question 5a");
        //Question 5 a

        String aedesVsFlyArff = "AedesVsFly.arff";
        Instances aedesVsFly = loadData(aedesVsFlyArff);
        aedesVsFly.setClassIndex(aedesVsFly.numAttributes()-1);

        Instances[] splitData = wekaTools.splitData(aedesVsFly, 0.3);
        Instances aedesFlyTrain = splitData[0];
        Instances aedesFlyTest = splitData[1];

        //IBk classifier
        weka.classifiers.lazy.IBk IBk = new weka.classifiers.lazy.IBk();
        IBk.buildClassifier(aedesFlyTrain);
        System.out.println("IBk " + wekaTools.accuracy(IBk, aedesFlyTest));
        int[] predictedIBk = wekaTools.classifyInstances(IBk, aedesFlyTest);
        int[] actualIBk = wekaTools.getClassValues(aedesFlyTest);

        int[][] IBkConfusion = wekaTools.confusionMatrix(predictedIBk, actualIBk);
        System.out.println("IBk Confusion Matrix");
        System.out.println("  0 1");
        System.out.println("0 " + IBkConfusion[0][0] + " " + IBkConfusion[1][0]);
        System.out.println("1 " + IBkConfusion[0][1] + " " + IBkConfusion[1][1]);
        System.out.println("IBk Confusion Matrix: " + Arrays.deepToString(IBkConfusion));

        //IB1 classifier
        weka.classifiers.lazy.IBk IB1 = new weka.classifiers.lazy.IBk(1);
        IB1.buildClassifier(aedesFlyTrain);
        System.out.println("IB1 " + wekaTools.accuracy(IB1, aedesFlyTest));
        int[] predictedIB1 = wekaTools.classifyInstances(IB1, aedesFlyTest);
        int[] actualIB1 = wekaTools.getClassValues(aedesFlyTest);

        int[][] IB1Confusion = wekaTools.confusionMatrix(predictedIB1, actualIB1);
        System.out.println("IB1 Confusion Matrix");
        System.out.println("  0 1");
        System.out.println("0 " + IB1Confusion[0][0] + " " + IB1Confusion[1][0]);
        System.out.println("1 " + IB1Confusion[0][1] + " " + IB1Confusion[1][1]);
        System.out.println("IB1 Confusion Matrix: " + Arrays.deepToString(IB1Confusion));

        //Logistic Regression classifier
        weka.classifiers.functions.Logistic lR = new weka.classifiers.functions.Logistic();
        lR.buildClassifier(aedesFlyTrain);
        System.out.println("lR " + wekaTools.accuracy(lR, aedesFlyTest));
        int[] predictedlR = wekaTools.classifyInstances(IBk, aedesFlyTest);
        int[] actuallR = wekaTools.getClassValues(aedesFlyTest);

        int[][] lRConfusion = wekaTools.confusionMatrix(predictedlR, actuallR);
        System.out.println("lR Confusion Matrix");
        System.out.println("  0 1");
        System.out.println("0 " + lRConfusion[0][0] + " " + lRConfusion[1][0]);
        System.out.println("1 " + lRConfusion[0][1] + " " + lRConfusion[1][1]);
        System.out.println("lR Confusion Matrix: " + Arrays.deepToString(lRConfusion));

        //zeroR classifier
        weka.classifiers.rules.ZeroR zeroR = new weka.classifiers.rules.ZeroR();
        zeroR.buildClassifier(aedesFlyTrain);
        System.out.println("Zero " + wekaTools.accuracy(lR, aedesFlyTest));
        int[] predictedZero = wekaTools.classifyInstances(zeroR, aedesFlyTest);
        int[] actualZero = wekaTools.getClassValues(aedesFlyTest);

        int[][] zeroRConfusion = wekaTools.confusionMatrix(predictedZero, actualZero);
        System.out.println("Zero Confusion Matrix");
        System.out.println("  0 1");
        System.out.println("0 " + zeroRConfusion[0][0] + " " + zeroRConfusion[1][0]);
        System.out.println("1 " + zeroRConfusion[0][1] + " " + zeroRConfusion[1][1]);
        System.out.println("Zero Confusion Matrix: " + Arrays.deepToString(zeroRConfusion));
    }

    public static void lab3Question5b(WekaTools wekaTools) throws Exception {
        System.out.println("Question 5b");
        //Question 5 b

        BufferedWriter br = new BufferedWriter(new FileWriter("aedesVsFlyClassifiers.csv"));
        StringBuilder sb = new StringBuilder();

        String aedesVsFlyArff = "AedesVsFly.arff";
        Instances aedesVsFly = loadData(aedesVsFlyArff);
        aedesVsFly.setClassIndex(aedesVsFly.numAttributes()-1);

        sb.append("IBK, IB1, lR, ZeroR\n");
        for (int i = 0; i<30; i++){
            String[] results = runThrough(wekaTools, aedesVsFly);

            for (int x = 0; x<4; x++) {
                sb.append(results[x]);
                if(x!=3) {
                    sb.append(",");
                }
            }
            sb.append("\n");

        }

        br.write(sb.toString());
        br.close();

    }

    public static String[] runThrough(WekaTools wekaTools, Instances instances) throws Exception {
        String[] results = new String[4];
        Instances[] splitData = wekaTools.splitData(instances, 0.3);
        Instances instancesTrain = splitData[0];
        Instances instancesTest = splitData[1];

        //IBk classifier
        weka.classifiers.lazy.IBk IBk = new weka.classifiers.lazy.IBk();
        IBk.buildClassifier(instancesTrain);
        results[0] = ("" + wekaTools.accuracy(IBk, instancesTest));

        //IB1 classifier
        weka.classifiers.lazy.IBk IB1 = new weka.classifiers.lazy.IBk(1);
        IB1.buildClassifier(instancesTrain);
        results[1] = ("" + wekaTools.accuracy(IB1, instancesTest));

        //Logistic Regression classifier
        weka.classifiers.functions.Logistic lR = new weka.classifiers.functions.Logistic();
        lR.buildClassifier(instancesTrain);
        results[2] = ("" + wekaTools.accuracy(lR, instancesTest));

        //zeroR classifier
        weka.classifiers.rules.ZeroR zeroR = new weka.classifiers.rules.ZeroR();
        zeroR.buildClassifier(instancesTrain);
        results[3] = ("" + wekaTools.accuracy(lR, instancesTest));

        return results;
    }

    public static Instances loadData(String filePath){
        Instances instance = null;

        try{
            FileReader reader = new FileReader(filePath);
            instance = new Instances(reader);
        }catch(Exception e){
            System.out.println("Exception caught: "+e);
        }
        return instance;
    }
}
